import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

// mock for resize-observer
global.ResizeObserver = jest.fn().mockImplementation(() => {
  return {
    observe: jest.fn(),
    unobserve: jest.fn(),
  }
})

configure({ adapter: new Adapter() })

global.console.error = error => {
  throw new Error(error)
}
